"""Account management serializers.

serializers.py
Worked on by: Tom Dryer (Team 1, Group A)
"""

from django.contrib.auth.models import User
from drf_compound_fields.fields import ListField
from rest_framework import serializers


class LoginSerializer(serializers.Serializer):
    """Serializer for login credentials."""

    email = serializers.EmailField()
    password = serializers.CharField()


class ChangeSessionSerializer(serializers.Serializer):
    """Serializer for changing password or profile name."""

    old_password = serializers.CharField(required=False)
    new_password = serializers.CharField(required=False)
    # We're limited to 30 chars because of Django's User model.
    name = serializers.CharField(required=False, max_length=30)


class EmailSerializer(serializers.Serializer):
    """Serializer for an email address."""

    email = serializers.EmailField()


class UserSerializer(serializers.ModelSerializer):
    """Serializer for a User."""

    name = serializers.CharField(source='first_name')
    email = serializers.CharField(source='username')

    class Meta:
        model = User
        fields = ('id', 'name', 'email')


class NewCourseSerializer(serializers.Serializer):
    """Serializer for new courses."""

    name = serializers.CharField()
    # TODO: Make this allow empty lists:
    student_emails = ListField(serializers.EmailField())


class ModifyCourseSerializer(serializers.Serializer):
    """Serializer for modifying courses."""

    # TODO: Make this allow empty lists:
    student_emails = ListField(serializers.EmailField())


class CourseSerializer(serializers.Serializer):
    """Serializer for a course."""

    id = serializers.IntegerField()
    name = serializers.CharField()
    instructor = serializers.EmailField()
    student_emails = ListField(serializers.EmailField())

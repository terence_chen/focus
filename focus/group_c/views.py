#from django.shortcuts import render

# Create your views here.

from django.http import HttpResponse
from django.utils import simplejson
from django.core import serializers

from group_c.models import (Question, Answer)

class QuestionManager():
	def getQuestion(self):
		questions = Question.objects.all()
		questionJS = [{"ID": q.id, "title": q.content, "date": str(q.creation_date), "activation": q.activated} for q in questions]
		return HttpResponse(simplejson.dumps(questionJS))

	def addQuestion(self, content):
		q = Question(content = content)
		q.save()
		questions = Question.objects.all().order_by("-id")
		questionJS = [{"ID": q.id, "title": q.content, "date": str(q.creation_date), "activation": q.activated} for q in questions]
		return HttpResponse(simplejson.dumps(questionJS))

	def deleteQuestion(self, id):
		q = Question.objects.get(id = id)
		q.delete()
		a = Answer.objects.filter(qid = id)
		a.delete()
		questions = Question.objects.all()
		questionJS = [{"ID": q.id, "title": q.content, "date": str(q.creation_date), "activation": q.activated} for q in questions]
		return HttpResponse(simplejson.dumps(questionJS))

	def activateQuestion(self, qid):
		q = Question.objects.get(id = qid)
		q.activated = True
		q.save()
		questions = Question.objects.all()
		questionJS = [{"ID": q.id, "title": q.content, "date": str(q.creation_date), "activation": q.activated} for q in questions]
		return HttpResponse(simplejson.dumps(questionJS))
	
	def deactivateQuestion(self, qid):
		q = Question.objects.get(id = qid)
		q.activated = False
		q.save()
		questions = Question.objects.all()
		questionJS = [{"ID": q.id, "title": q.content, "date": str(q.creation_date), "activation": q.activated} for q in questions]
		return HttpResponse(simplejson.dumps(questionJS))

	def modifyQuestion(self, qid, content):
		q = Question.objects.get(id = qid)
		q.content = content
		q.save()
		questions = Question.objects.all()
		questionJS = [{"ID": q.id, "title": q.content, "date": str(q.creation_date), "activation": q.activated} for q in questions]
		return HttpResponse(simplejson.dumps(questionJS))

class AnswerManager():
	def getAnswer(self):
		answers = Answer.objects.all()
		answerJS = [{"Question": ((Question.objects.get(id = q.qid)).content), "content": q.content, "AnswerNumber": q.number} for q in answers]
		#answerJS = [{"Question": q.qid, "content": q.content, "AnswerNumber": q.number} for q in answers]
		return HttpResponse(simplejson.dumps(answerJS))

	def getSpecificAnswer(self, qid):
		answers = Answer.objects.filter(qid = qid)
		answerJS = [{"Number": q.number, "Answer": q.content} for q in answers]
		return HttpResponse(simplejson.dumps(answerJS))		
	
	def addAnswer(self, number, content):
		q = Question.objects.all().order_by("-id")[0]
		qid = q.id
		a = Answer(qid = qid, number = number, content = content)
		a.save()
		answers = Answer.objects.all()
		answerJS = [{"ID": q.qid, "content": q.content, "number": q.number} for q in answers]
		return HttpResponse(simplejson.dumps(answerJS))

	def modifyAnswer(self, qid, number, modified_content):
		a = Answer.objects.get(qid = qid, number = number)
		a.content = modified_content
		a.save()
		answers = Answer.objects.all()
		answerJS = [{"ID": q.qid, "content": q.content, "number": q.number} for q in answers]
		return HttpResponse(simplejson.dumps(answerJS))

	def getData(self):
		questions = Question.objects.all().order_by("id")
		dataJS = [{"title": q.content, "answer": [{"ans": a.content} for a in Answer.objects.filter(qid = q.id).order_by("id")], "activated": q.activated, "id": q.id} for q in questions]
		return HttpResponse(simplejson.dumps(dataJS))

	def addData2(self, qcontent, ans1, ans2):
		q = Question(content = qcontent)
		q.save()
		qid = Question.objects.get(content = qcontent).id
		a = Answer(qid = qid, content = ans1, number = 1)
		a.save()
		b = Answer(qid = qid, content = ans2, number = 2)
		b.save()
		answers = Answer.objects.all()
		answerJS = [{"Question": ((Question.objects.get(id = q.qid)).content), "content": q.content, "AnswerNumber": q.number} for q in answers]
		return HttpResponse(simplejson.dumps(answerJS))
	
	def addData3(self, qcontent, ans1, ans2, ans3):
		q = Question(content = qcontent)
		q.save()
		a = Answer(qid = q.id, content = ans1, number = 1)
		b = Answer(qid = q.id, content = ans2, number = 2)
		c = Answer(qid = q.id, content = ans3, number = 3)
		a.save()
		b.save()
		c.save()
		answers = Answer.objects.all()
		answerJS = [{"Question": ((Question.objects.get(id = q.qid)).content), "content": q.content, "AnswerNumber": q.number} for q in answers]
		return HttpResponse(simplejson.dumps(answerJS))
	
	def addData4(self, qcontent, ans1, ans2, ans3, ans4):
		q = Question(content = qcontent)
		q.save()
		a = Answer(qid = q.id, content = ans1, number = 1)
		b = Answer(qid = q.id, content = ans2, number = 2)
		c = Answer(qid = q.id, content = ans3, number = 3)
		d = Answer(qid = q.id, content = ans4, number = 4)
		a.save()
		b.save()
		c.save()
		d.save()
		answers = Answer.objects.all()
		answerJS = [{"Question": ((Question.objects.get(id = q.qid)).content), "content": q.content, "AnswerNumber": q.number} for q in answers]
		return HttpResponse(simplejson.dumps(answerJS))
	
	def addData5(self, qcontent, ans1, ans2, ans3, ans4, ans5):
		q = Question(content = qcontent)
		q.save()
		a = Answer(qid = q.id, content = ans1, number = 1)
		b = Answer(qid = q.id, content = ans2, number = 2)
		c = Answer(qid = q.id, content = ans3, number = 3)
		d = Answer(qid = q.id, content = ans4, number = 4)
		e = Answer(qid = q.id, content = ans5, number = 5)
		a.save()
		b.save()
		c.save()
		d.save()
		e.save()
		answers = Answer.objects.all()
		answerJS = [{"Question": ((Question.objects.get(id = q.qid)).content), "content": q.content, "AnswerNumber": q.number} for q in answers]
		return HttpResponse(simplejson.dumps(answerJS))


'''
class SubmissionManager():
	def getSubmission(self):
		submissions = Submission.objects.all()
		submissionJS = [{"student": q.student, "question": q.content, "answer": q.number} for q in answers]
		return HttpResponse(simplejson.dumps(submissionJS))
	
	def addSubmission(self, qid, number, content):
		a = Submission(qid = qid, number = number, content = content)
		a.save()
		submissions = Submission.objects.all()
		submissionJS = [{"ID": q.qid, "content": q.content, "number": q.number} for q in answers]
		return HttpResponse(simplejson.dumps(submissionJS))
'''

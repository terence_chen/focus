"""Root URL configuration.

urls.py
Worked on by: Tom Dryer (Team 1, Group A)
"""

from django.conf import urls, settings
from django.contrib import staticfiles
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

import focus.views


urlpatterns = urls.patterns(
    '',

    # Example API endpoint
    urls.url(r'^api/endpoint.json', focus.views.example_endpoint),

    # Import API endpoints for each feature.
    (r'^api/accounts/', urls.include('accounts.urls')),
    (r'^api/group_b/', urls.include('group_b.urls')),
    (r'^api/group_c/', urls.include('group_c.urls')),
    (r'^api/file_management/', urls.include('file_management.urls')),
    (r'^api/group_e/', urls.include('group_e.urls')),
    (r'^media/(?P<path>.*)$', 'django.views.static.serve', {
        'document_root': settings.MEDIA_ROOT}),

)


if settings.DEBUG:
    # Serve static files under /static/.
    urlpatterns += staticfiles_urlpatterns()
    # Support AngularJS in HTML5 mode by serving various URLs to our
    # index.html.
    urlpatterns += urls.url(r'^.*$',
                            lambda r: staticfiles.views.serve(r, 'index.html')),

from django.contrib import admin
from .models import *
# Register your models here.

admin.site.register(Quiz)
admin.site.register(Question)
admin.site.register(QuestionAnswer)
admin.site.register(StudentQuiz)
admin.site.register(StudentAnswer)

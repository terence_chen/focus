"""models.py

Description: Django database model for upload features

Team 1 Group D 

Created by: Kelvin Lau, 2014-06-17

Programmers: Derek Ho, Kelvin Lau

Changes: 
2014-07-01 Delete API from database and file servers
2014-07-02 Added title into the model database
2014-07-04 Fixed database model 
2014-07-06 Added filesize, filename and filetype. Removed Ttile
2014-07-21 Added the course_id for reference 
2014-07-22 Changed course_id to foreign key field 

Known bugs: 
None
"""
from django.db import models

class Document(models.Model):
    """model with title of file, filepath, publication date"""
    file = models.FileField(upload_to='uploads')
    pub_date = models.DateField(auto_now_add=True)
    filename = models.CharField(max_length=200)
    filesize = models.CharField(max_length=32)
    filetype = models.CharField(max_length=200)
    course_id = models.ForeignKey('accounts.Course')

    """delete function required to delete the file from the server when requested"""
    def delete(self, *args, **kwargs):
        self.file.delete(False)
        super(Document, self).delete(*args, **kwargs)


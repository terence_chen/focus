""" serializers.py

Description: serializer class to seralizer/deserialize the data objects

Team 1 Group D 

Created by: Kelvin Lau, 2014-06-17

Programmers: Kelvin Lau, Derek Ho

Changes: 
2014-07-04 Changged Serailizer type
2014-07-06 Fixed proper serializer 

Known bugs: 
None
"""
from rest_framework import serializers
from file_management.models import Document

"""serializer class for seralizier data between endpoint calls"""
class FileUploadSerializer(serializers.ModelSerializer):
    class Meta:
        model = Document
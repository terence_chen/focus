""" views.py

Description: views for the django REST API

Team 1 Group D

Created by: Kelvin Lau, 2014-06-17

Programmers: Kelvin Lau, Derek Ho

Changes:
2014-06-29 Added API to get file from the server
2014-07-01 Added API To delete file from the server and database
2014-07-02 Added title field  for uploaded files
2014-07-04 Fixed features for uploading files
2014-07-04 Changed the view to class based, changed upload types
2014-07-05 Added comments and fixed identation for the code
2014-07-05 Added file size, filename and file type. Remove title
2014-07-05 Added validation in file size for server side
2014-07-21 Added the course_id paramter for get request in FileUploadView


Known bugs:
None
"""

from django.shortcuts import render_to_response, get_object_or_404
from django.views.decorators.csrf import csrf_exempt
from rest_framework.decorators import api_view, parser_classes
from rest_framework.exceptions import ParseError
from rest_framework.response import Response
from rest_framework.views import APIView

from file_management.serializers import FileUploadSerializer
from file_management.models import Document



class FileUploadView(APIView):

    def post(self, request):
        """Uploads a file

        Takes file paramter with data and title name

        Returns 201 if successful, upload file and create record with informartion in the database

        Returns 400 if request is invalid.

        """
        fileSize = request.DATA['filesize']
        max_file_size = 41943040 #40 MB limit
        serializer = FileUploadSerializer(data=request.DATA, files=request.FILES)
        if (int(fileSize) > max_file_size):
            return Response(status=400)
        elif serializer.is_valid():
            serializer.save()
            return Response(status=201)
        else:
            raise ParseError(detail=serializer.errors)



class GetFileUpload(APIView):

    def get(self, request, course_id):
        """Gets a list of all files

        Does not take paramter, passes all the files

        Returns 200 if successful

        Returns 400 if request is invalid.

        """
        files = Document.objects.filter(course_id=course_id)
        serializer = FileUploadSerializer(files, many=True)
        return Response(serializer.data)
 

class FileDeleteView(APIView):

    def delete(self, request, id):
        """Deletes file from server and removes database records

        Takes paramter of the primary key of the record

        Returns 204 if successful

        """
        document = get_object_or_404(Document, pk=id)
        document.file.delete(save=False)
        document.delete()
        return Response(status=204)


/* FileManagementCtrl.js

Description: File management controller for the angular JS single page application

Team 1 Group D

Created by: Derek Ho, 2014-07-02

Programmers: Derek Ho, Kelvin Lau

Changes:
2014-07-03  Fixed Some angular control to read files from template
2014-07-05  Added file manager to upload and delete file manager
2014-07-21  Changed the controller to pass course.id to backend to Store Foreign Key
2014-07-22  Changed syntax and other things

Known bugs:
None
*/
(function() {

  this.controller('FileManagementCtrl', function($scope, course, FileManager) {
    $scope.course = course;
    $scope.files = FileManager.getFiles(course.id).$object;

    $scope.uploadFile = function(file) {
      FileManager.uploadFile(file, course.id).then(function() {
        $scope.files = FileManager.getFiles(course.id).$object;
      });
    };
    $scope.deleteFile = function(file, index) {
      FileManager.deleteFile(file).then(function() {
        $scope.files.splice(index, 1);
      });
    };
  });

  this.directive('uploadfile', function() {
    return {
      require: 'ngModel', // must have ng-model attr
      link: function(scope, elem, attrs) {
        // When the input changes (aka, when the guy selects a file) ...
        elem.bind('change', function(ev) {
          var file = ev.target.files[0]
            , model_name = attrs.ngModel;
          // when the reader gets data ...

          scope.$apply(function() {
            scope[model_name] = file;
          });
        });
      }
    }
  });

}).call(angular.module('controllers'));

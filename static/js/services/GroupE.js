(function() {

  this.factory('GroupE', function(Restangular) {

  	attendanceData = {
    id: 1, 
    class_id: 1, 
    timestamp: "", 
    code: Math.floor(Math.random()*9000) + 1000,
    expiration_time: 600
	};

  	/*
       Return Record Based on all calls
	*/

    function getStats(course_id){
        //need timestamp
      return Restangular.one('group_e/Stats/?status=1&timestamp=2014-07-26&class_id='+course_id).get();
    }
    function getAttendance(course_id) {
      return Restangular.one('group_e/Attendance/?format=json&class_id='+course_id).get();
    }



    function setAttendance(timestamp,course_id){
      //check if there is one today, if there is update it other wise insert
        
      attendanceData.timestamp = timestamp;
        console.log(timestamp);
      attendanceData.class_id = course_id;
        getData = Restangular.one('group_e/AttStamp/?class_id='+course_id).get();
        try{
          if(!getData[0]){
            popData = Restangular.one('group_e/Attendance_Populate/?class_id='+course_id+'&timestamp='+timestamp).get();
          }
        }catch(e){
            console.log(e);
        }
      inData = Restangular.one('group_e/Attendance_Check/?timestamp='+timestamp+'&class_id='+course_id).get();
      
      //console.log(inData[6]);

      //if data exists just update row, other wise post
      if(inData["id"]){
         pk = inData["id"];
         console.log("kenny");
         console.log(inData);
         return Restangular.one('group_e/Attendance_Check/'+pk+'/').customPUT(attendanceData);
      }
    	
    	return Restangular.one('group_e/Attendance_Check/').customPOST(attendanceData);

    }

    function changeStatus(id,studentId,classId,timeStamp,status){
        testData = {
        "id": id, 
        "student_id": studentId, 
        "class_id": classId, 
        "timestamp": timeStamp, 
        "status": status
      };
      return Restangular.one('group_e/Attendance/'+id+'/').customPUT(testData);
    }

    return {
      getAttendance: getAttendance, setAttendance: setAttendance, getStats: getStats, changeStatus:changeStatus
    };
  });

}).call(angular.module('services'));

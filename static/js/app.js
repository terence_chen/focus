/**
 * `static/js/app.js`
 *
 * Worked on by: David Yan (Team 1, Group A)
 *   2014-06-10: Created AngularJS shell for other groups to use
 *   2014-06-11: Enabled HTML5Mode now that Django serves static asset properly
 *   2014-06-22: Added routes for Login
 *   2014-07-02: Refactored the way login/logout works (now via event emission)
 *   2014-07-06: Added routes for course editing
 *   2014-07-09: Added routes for profile
 *   2014-07-19: Added login/logout to navbar
 *   2014-07-19: Fixed `/` route from 404-ing if user is not logged in
 *   2014-07-20: Removed sidebar, opting to put links on the top nav
 *   2014-07-20: Added routes for logout
 *   2014-07-20: Added session code to restrict routes based on permission
 *   2014-07-21: Moved file management to a per course scheme
 *   2014-07-23: Moved attendance to a per course scheme
 *   2014-07-25: Moved class participation to a per course scheme
 *
 * Worked on by: Jaime To (Team 1, Group A)
 *   2014-06-25: Added routes for Create Instructor
 *   2014-06-25: Added routes for Create Course
 *   2014-07-17: Added Angular-ui component
 *   2014-07-18: Added Directives
 *   2014-07-18: Change main page to create course view
 */

(function() {

  // Allow the app to use native route (/route) instead of fake route (/#route)
  this.config(function($locationProvider) {
    $locationProvider.html5Mode(true);
  });

  /**
   * Defines every route of the app
   *
   * The `controller` attribute needs to be a defined controller in a JS file in
   * static/js/controllers
   *
   * The `templateUrl` attribute needs to be a HTML template file in
   * static/partials
   *
   * The `resolve` attribute are a set of promises that must return succeed
   * before the route is allow to be accessed, and once all promises fulfilled,
   * the data from the promise is extracted and passed onto the controller,
   * named by the key of the resolve element.
   * See:
   * http://www.html5rocks.com/en/tutorials/es6/promises/
   * https://docs.angularjs.org/api/ngRoute/provider/$routeProvider
   */
  this.config(function($routeProvider) {
    $routeProvider.when('/', {
      controller: 'IndexCtrl',
      templateUrl: '/static/partials/index.html'
    });

    $routeProvider.when('/404', {
      controller: '404Ctrl',
      templateUrl: '/static/partials/404.html'
    });

    $routeProvider.when('/login', {
      controller: 'LoginCtrl',
      templateUrl: '/static/partials/login.html'
    });

    $routeProvider.when('/logout', {
      templateUrl: '/static/partials/logout.html',
      resolve: {
        permission: function($q, User) {
          if (User.isLoggedIn) {
            return $q.reject();
          }
        }
      }
    });

    $routeProvider.when('/profile', {
      controller: 'ProfileCtrl',
      templateUrl: '/static/partials/profile.html',
      resolve: {
        permission: function($q, User) {
          if (!User.isLoggedIn) {
            return $q.reject();
          }
        }
      }
    });

    $routeProvider.when('/create-instructor', {
      controller: 'InstructorCtrl',
      templateUrl: '/static/partials/create-instructor.html',
      resolve: {
        permission: function($q, User) {
          if (!User.session) {
            return User.getSession().then(function(data) {
              if (!data.is_admin) {
                return $q.reject();
              }
            });
          }
        },
        instructors: function($q, Instructor) {
          return Instructor.getInstructors();
        }
      }
    });

    $routeProvider.when('/course/:id', {
      controller: 'CourseCtrl',
      templateUrl: '/static/partials/course.html',
      resolve: {
        permission: function($q, User) {
          if (!User.session) {
            return User.getSession().then(function(data) {
              if (!data.is_instructor) {
                return $q.reject();
              }
            });
          }
        },
        course: function($route, Course) {
          return Course.getCourse($route.current.params.id);
        }
      }
    });

    $routeProvider.when('/course/:id/quizzes', {
      controller: 'GroupBCtrl',
      templateUrl: '/static/partials/group-b-tmpl.html',
      resolve: {
        permission: function($q, User) {
          if (!User.session) {
            return User.getSession().then(function(data) {
              if (!data.is_instructor) {
                return $q.reject();
              }
            });
          }
        },
        course: function($route, Course) {
          return Course.getCourse($route.current.params.id);
        }
      }
    });

    $routeProvider.when('/course/:id/quizzes/create-quiz', {
      controller: 'CreateQuizCtrl',
      templateUrl: '/static/partials/createquiz.html',
      resolve: {
        permission: function($q, User) {
          if (!User.session) {
            return User.getSession().then(function(data) {
              if (!data.is_instructor) {
                return $q.reject();
              }
            });
          }
        },
        course: function($route, Course) {
          return Course.getCourse($route.current.params.id);
        }
      }
    });

    $routeProvider.when('/course/:id/quizzes/edit-quiz/:quizId', {
      controller: 'EditQuizCtrl',
      templateUrl: '/static/partials/editquiz.html',
      resolve: {
        permission: function($q, User) {
          if (!User.session) {
            return User.getSession().then(function(data) {
              if (!data.is_instructor) {
                return $q.reject();
              }
            });
          }
        },
        course: function($route, Course) {
          return Course.getCourse($route.current.params.id);
        },
        quiz: function($route, $q, Quiz) {
          return Quiz.getQuiz().then(function(res) {
			var result;
			_.each(res, function(val, key) {
				if (val.id == $route.current.params.quizId) {
				result = val;
				}
			});
			if (!result) {
				return $q.reject();
			}
			return result;
			// console.log(res[]);
		  });
        }
      }
    });
	
    $routeProvider.when('/course/:id/participation', {
      controller: 'GroupCCtrl',
      templateUrl: '/static/partials/group-c-tmpl.html',
      resolve: {
        permission: function($q, User) {
          if (!User.session) {
            return User.getSession().then(function(data) {
              if (!data.is_instructor) {
                return $q.reject();
              }
            });
          }
        },
        course: function($route, Course) {
          return Course.getCourse($route.current.params.id);
        }
      }
    });

    $routeProvider.when('/course/:id/file-management', {
      controller: 'FileManagementCtrl',
      templateUrl: '/static/partials/file-management-tmpl.html',
      resolve: {
        permission: function($q, User) {
          if (!User.session) {
            return User.getSession().then(function(data) {
              if (!data.is_instructor) {
                return $q.reject();
              }
            });
          }
        },
        course: function($route, Course) {
          return Course.getCourse($route.current.params.id);
        }
      }
    });

    $routeProvider.when('/course/:id/attendance', {
      controller: 'GroupECtrl',
      templateUrl: '/static/partials/group-e-tmpl.html',
      resolve: {
        permission: function($q, User) {
          if (!User.session) {
            return User.getSession().then(function(data) {
              if (!data.is_instructor) {
                return $q.reject();
              }
            });
          }
        },
        course: function($route, Course) {
          return Course.getCourse($route.current.params.id);
        }
      }
    });

    $routeProvider.otherwise({
      redirectTo: '/404'
    });
  });

  // All API endpoints are prefixed with `api`
  // For example: localhost:8000/api/accounts/endpoint
  this.config(function(RestangularProvider) {
    RestangularProvider.setBaseUrl('/api');
  });

  // A skeleton HTTP interceptor, for possible future use
  // For example, when an API hit is made, display global an AJAX spinner, and
  // when the hit is finished, reenable the UI
  this.config(function($httpProvider) {
    $httpProvider.interceptors.push(function($q) {
      return {
        request: function(config) {
          return config;
        },
        requestError: function(rejection) {
          return $q.reject(rejection);
        },
        response: function(response) {
          return response;
        },
        responseError: function(rejection) {
          return $q.reject(rejection);
        }
      };
    });
  });

  // Configuration options for when Angular initializes the app.
  this.run(function($rootScope, $location, User, Course, Restangular, Cookie) {

    // Takes the `csrftoken` cookie that Django sets and adds it to the header
    function updateHeader() {
      Restangular.setDefaultHeaders({
        'X-CSRFToken': Cookie.get('csrftoken')
      });
    }

    function updateSession() {
      User.getSession().then(function(data) {
        User.session = data;
      }, function() {
        User.session = false;
      });
    }

    // Expose the logout functionality app-wide, to be used in any tmpl context
    $rootScope.User = User;
    $rootScope.Course = Course;

    $rootScope.logout = function() {
      User.logout().then(function() {
        $location.path('/logout');
      });
    };

    // Event emission for login and logout. Basically, whenever either event
    // occurs, update the `csrftoken` in the API endpoint hit headers.
    $rootScope.$on('$login', function() {
      updateHeader();
      User.isLoggedIn = true;
      updateSession();
    });
    $rootScope.$on('$logout', function() {
      updateHeader();
      User.isLoggedIn = false;
      User.session = false;
    });
    updateHeader(); // update the header when the app first runs

    if (User.isLoggedIn) {
      updateSession();
    }

    $rootScope.$on('$course', function(ev, course) {
      Course.course = course;
    });

    // Below are skeleton events for route changesm for possible future use
    // For example, when the user changes routes, we can show some sort of
    // animation and what not
    $rootScope.$on('$routeChangeStart', function() {
    });

    $rootScope.$on('$routeChangeError', function() {
      // If any resolve objects fail, redirect to 404
      $location.path('/404');
    });

    $rootScope.$on('$routeChangeSuccess', function() {
      if ($location.path().substring(0, 8) !== '/course/') {
        Course.course = false;
      }
    });
  });

// App dependencies are passed as an arugment into the above IIFE. See:
// http://en.wikipedia.org/wiki/Immediately-invoked_function_expression
}).call(angular.module('app', [
  'restangular', // Restangular is a helpful lib for hitting REST APIs
  'ngRoute', // ngRoutes is what enables us to have custom routes (URLs)
  'ngSanitize', // For possible future use when displaying User-created content
  'controllers', // Our controllers, defined in static/js/controllers
  'services', // Our services/models, defined in static/js/services
  'ngAnimate',
  'toaster', // used for toast notifications (https://github.com/jirikavi/AngularJS-Toaster)
  'ui.bootstrap', // Angular directives based on Bootstrap
]));
